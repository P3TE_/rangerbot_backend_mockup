package radioserver;

import radioserver.connection.TcpServer;
import radioserver.connection.message.InvalidMessageException;
import radioserver.connection.message.MessageBase;
import radioserver.connection.message.fromauv.AuvsmMessage;
import radioserver.connection.message.toauv.AuvcmMessage;
import radioserver.connection.message.toauv.AuvjmMessage;
import radioserver.testmode.TestMode;
import radioserver.testmode.TestModeType;

import java.util.Scanner;

public class Main {

    public static final int SERVER_PORT = 9876;

    public static void main(String[] args) {

        verifyMessagesAreConsistent();

        System.out.println("Testing server for the rangerbot 900MHz radio.");

        Scanner userInput = new Scanner(System.in);
        TestModeType testModeType = TestModeType.getTestModeFromUseringInput(userInput);

        System.out.println();
        System.out.println("Starting server on port: " + SERVER_PORT);
        TcpServer tcpServer = new TcpServer(SERVER_PORT);
        Thread tcpServerThread = new Thread(tcpServer);
        tcpServerThread.start();

        TestMode testMode = testModeType.generateTestMode(tcpServer);
        Thread testModeThread = new Thread(testMode);
        testModeThread.start();

        while (true) {
            String userLine = userInput.nextLine().trim();
            if (userLine.equalsIgnoreCase("stop")) {
                System.exit(0);
            }
        }
    }

    private static void verifyMessagesAreConsistent() {

        verfyMessage(new AuvcmMessage(2, AuvcmMessage.AuvAction.START));
        verfyMessage(new AuvjmMessage(4, 20, 50));
        verfyMessage(new AuvsmMessage(1, 34.6315634, 12.2355313, 24.2, 18.6,
                AuvsmMessage.MissionState.PAUSED, AuvsmMessage.ErrorState.GPS_ERROR));

    }

    private static void verfyMessage(MessageBase messageBase) {
        try {
            MessageBase duplicate = MessageBase.parseReceivedMessage(messageBase.buildMessage());
            if (!messageBase.equals(duplicate)) {
                System.err.println("MessageBase didn't encode/decode correctly...");
            }
        } catch (InvalidMessageException e) {
            e.printStackTrace();
        }
    }
}
