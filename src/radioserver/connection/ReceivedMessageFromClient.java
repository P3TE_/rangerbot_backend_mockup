package radioserver.connection;

public class ReceivedMessageFromClient {

    private long receivedTime;
    private String message;

    public ReceivedMessageFromClient(long receivedTime, String message) {
        this.receivedTime = receivedTime;
        this.message = message;
    }

    public long getReceivedTime() {
        return receivedTime;
    }

    public void setReceivedTime(long receivedTime) {
        this.receivedTime = receivedTime;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
