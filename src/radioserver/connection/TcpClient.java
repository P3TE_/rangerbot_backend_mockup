package radioserver.connection;

import radioserver.connection.message.InvalidMessageException;
import radioserver.connection.message.MessageBase;
import radioserver.dummyuav.DummyAuv;
import radioserver.dummyuav.DummyAuvHandler;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.*;

public class TcpClient implements Runnable {

    private final Socket connectionSocket;
    private final BufferedReader inFromClient;
    private final DataOutputStream outToClient;
    private final TcpServer tcpServer;
    private volatile boolean running = true;

    private static final long MESSAGE_HISTORY_SIZE = 2000; //ms
    private final int MAX_MESSAGES_IN_HISTORY_TIME = 6;
    private Deque<ReceivedMessageFromClient> messagesFromClientInHistory = new LinkedList<>();

    public TcpClient(Socket connectionSocket, BufferedReader inFromClient, DataOutputStream outToClient, TcpServer tcpServer) {
        this.connectionSocket = connectionSocket;
        this.inFromClient = inFromClient;
        this.outToClient = outToClient;
        this.tcpServer = tcpServer;
    }


    @Override
    public void run() {
        while (running) {
            String lineFromClient = null;
            try {
                lineFromClient = inFromClient.readLine();
                if (lineFromClient == null) {
                    this.running = false;
                    continue;
                }
            } catch (IOException e) {
                this.running = false;
                continue;
            }
            parseMessage(lineFromClient);
        }
        System.out.println("Client disconnected with ip: " + connectionSocket.getInetAddress());
    }

    private void parseMessage(String receivedMessage) {
        receivedMessage = receivedMessage.trim();

        long currentTime = System.currentTimeMillis();
        long currentTimeMinusTwoSeconds = currentTime - MESSAGE_HISTORY_SIZE;
        Iterator<ReceivedMessageFromClient> receivedMessageFromClientIterator = messagesFromClientInHistory.iterator();
        while (receivedMessageFromClientIterator.hasNext()) {
            ReceivedMessageFromClient receivedMessageFromClient = receivedMessageFromClientIterator.next();
            if (receivedMessageFromClient.getReceivedTime() < currentTimeMinusTwoSeconds) {
                receivedMessageFromClientIterator.remove();
            }
        }

        messagesFromClientInHistory.addLast(new ReceivedMessageFromClient(currentTime, receivedMessage));

        if (messagesFromClientInHistory.size() > MAX_MESSAGES_IN_HISTORY_TIME) {
            System.err.println("WARNING: have received " + messagesFromClientInHistory.size() +
                    " messages in the last " + (((double) MESSAGE_HISTORY_SIZE) / 1000.0) +
                    " seconds. Please limit this to no more than " + MAX_MESSAGES_IN_HISTORY_TIME);
        }


        try {
            MessageBase newMessage = MessageBase.parseReceivedMessage(receivedMessage);
            System.out.println("Valid message received: " + newMessage);
        } catch (InvalidMessageException e) {
            System.err.println("INVALID MESSAGE RECEIVED: " + e.getMessage());
        }


    }

    public Socket getConnectionSocket() {
        return connectionSocket;
    }

    public BufferedReader getInFromClient() {
        return inFromClient;
    }

    public DataOutputStream getOutToClient() {
        return outToClient;
    }

    public boolean isRunning() {
        return running;
    }

}
