package radioserver.connection;

import radioserver.connection.message.MessageBase;
import sun.awt.Mutex;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;

public class TcpServer implements Runnable {

    private int serverPort;

    public TcpServer(int serverPort) {
        this.serverPort = serverPort;
    }

    private ArrayList<TcpClient> allClients = new ArrayList<>();
    private Mutex allClientsMutex = new Mutex();

    private void handleServer() throws IOException {

        ServerSocket welcomeSocket = new ServerSocket(serverPort);

        while (true) {
            Socket connectionSocket = welcomeSocket.accept();

            System.out.println("Client connected with ip: " + connectionSocket.getInetAddress());

            BufferedReader inFromClient =
                    new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
            DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());

            TcpClient client = new TcpClient(connectionSocket, inFromClient, outToClient, this);
            Thread clientThread = new Thread(client);
            clientThread.start();
            allClientsMutex.lock();
            allClients.add(client);
            allClientsMutex.unlock();

        }
    }

    public void sendMessageToAllClients(MessageBase message) {
        allClientsMutex.lock();

        Iterator<TcpClient> tcpClientIterator = allClients.iterator();
        while (tcpClientIterator.hasNext()) {
            TcpClient client = tcpClientIterator.next();
            try {
                client.getOutToClient().write(message.buildMessage().getBytes());
                client.getOutToClient().flush();
            } catch (IOException e) {
                //e.printStackTrace();
                System.err.println("Client disconnected...");
                tcpClientIterator.remove();
            }
        }

        allClientsMutex.unlock();
    }

    @Override
    public void run() {
        try {
            handleServer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
