package radioserver.connection.message;

import radioserver.connection.message.fromauv.AuvsmMessage;
import radioserver.connection.message.toauv.AuvcmMessage;
import radioserver.connection.message.toauv.AuvjmMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * @author p3te
 */
public abstract class MessageBase {

    public MessageBase() {
    }

    public MessageBase(List<String> messageValues) throws InvalidMessageException {
    }

    protected abstract void generateMessageContentsWithoutId(StringBuilder messageBuilder);

    protected abstract String getMessageId();

    public static final MessageBase parseReceivedMessage(String receivedMessage) throws InvalidMessageException {

        //Get rid of any \r\n
        receivedMessage = receivedMessage.trim();

        if (receivedMessage.length() < 5) {
            throw new InvalidMessageException("Invalid message received, too short: " + receivedMessage);
        }

        char startSymbol = receivedMessage.charAt(0);
        char checksumSymbolLeastSignificantNibble = receivedMessage.charAt(receivedMessage.length() - 1);
        char checksumSymbolMostSignificantNibble = receivedMessage.charAt(receivedMessage.length() - 2);
        String receivedChecksumInHex = new String(new char[]{checksumSymbolMostSignificantNibble, checksumSymbolLeastSignificantNibble});
        char messageTerminatedStar = receivedMessage.charAt(receivedMessage.length() - 3);

        //TODO - verify.
        String innerMessage = receivedMessage.substring(1, receivedMessage.length() - 3);
        int checksum = generateChecksumFromString(innerMessage);
        String calculatedChecksumInHex = turnByteIntoHexCode(checksum);

        if (!receivedChecksumInHex.toUpperCase().equals(calculatedChecksumInHex.toUpperCase())) {
            throw new InvalidMessageException("Checksum did not match, expected: " + calculatedChecksumInHex + ", received: " + receivedChecksumInHex);
        }

        if (startSymbol != '$') {
            throw new InvalidMessageException("Expected message to start with a '$', instead it started with: " + startSymbol);
        }


        if (messageTerminatedStar != '*') {
            throw new InvalidMessageException("Expected message to end with a '*' (Before the checksum), instead it ended with: " + messageTerminatedStar);
        }

        if (innerMessage.length() == 0) {
            throw new InvalidMessageException("BAD MESSAGE RECEIVED: Received an empty message!");
        }

        List<String> receivedParameters = new ArrayList<>();
        StringBuilder currentParameter = new StringBuilder();
        for (int i = 0; i < innerMessage.length(); i++) {
            char c = innerMessage.charAt(i);

            if (c == ',') {
                receivedParameters.add(currentParameter.toString());
                currentParameter.delete(0, currentParameter.length());
            } else {
                currentParameter.append(c);
            }
        }
        receivedParameters.add(currentParameter.toString());


        List<String> nonMessageIdParameters = new ArrayList<>();
        for (int i = 1; i < receivedParameters.size(); i++) {
            nonMessageIdParameters.add(receivedParameters.get(i));
        }

        String messageId = receivedParameters.get(0);
        switch (messageId) {
            case AuvcmMessage.MESSAGE_ID:
                return new AuvcmMessage(nonMessageIdParameters);
            case AuvjmMessage.MESSAGE_ID:
                return new AuvjmMessage(nonMessageIdParameters);
            case AuvsmMessage.MESSAGE_ID:
                return new AuvsmMessage(nonMessageIdParameters);
            default:
                throw new InvalidMessageException("Unknown message with ID: " + messageId);
        }

    }

    public final String buildMessage() {
        StringBuilder result = new StringBuilder();

        //Append the message id:
        result.append("$");

        //Get the full message contents
        StringBuilder messageContentsBuilder = new StringBuilder();
        messageContentsBuilder.append(getMessageId());
        messageContentsBuilder.append(",");
        generateMessageContentsWithoutId(messageContentsBuilder);
        String messageContents = messageContentsBuilder.toString();
        int messageChecksum = generateChecksumFromString(messageContents);

        //Append the * and the checksum, and carriage return new line.
        result.append(messageContents);
        result.append("*");
        result.append(turnByteIntoHexCode(messageChecksum));
        result.append("\r\n");
        return result.toString();
    }



    public static int generateChecksumFromString(String wholeString) {
        return generateChecksumFromString(wholeString, 0, wholeString.length());
    }

    public static int generateChecksumFromString(String wholeString, int startIndex, int endIndex) {
        startIndex = Math.max(0, startIndex);
        endIndex = Math.min(wholeString.length(), endIndex);

        int result = 0;
        for (int i = startIndex; i < endIndex; i++) {
            result ^= ((int) wholeString.charAt(i));
        }
        return result;
    }

    public static String turnByteIntoHexCode(int value) {
        int mostSigNibble = (value & 0xF0) >> 4;
        int leastSigNibble = value & 0x0F;

        StringBuilder result = new StringBuilder();
        result.append(nibbleIntoChar(mostSigNibble));
        result.append(nibbleIntoChar(leastSigNibble));
        return result.toString();
    }

    /**
     * @require nibble >= 0 && nibble <= 15
     * @param nibble
     * @return
     */
    public static char nibbleIntoChar(int nibble) {
        if (nibble < 10) {
            return (char) ('0' + nibble);
        } else {
            return (char) ('A' + (nibble - 10));
        }
    }


    @Override
    public String toString() {
        return buildMessage();
    }
}
