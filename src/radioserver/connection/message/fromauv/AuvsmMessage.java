package radioserver.connection.message.fromauv;

import radioserver.connection.message.InvalidMessageException;
import radioserver.connection.message.MessageBase;

import java.util.List;

/**
 * @author p3te
 */
public class AuvsmMessage extends MessageBase {

    public static final String MESSAGE_ID = "AUVSM";

    @Override
    protected String getMessageId() {
        return MESSAGE_ID;
    }

    public enum MissionState {
        RUNNING("0"),
        STOPPED("1"),
        PAUSED("2");

        private final String missionStateId;

        MissionState(String missionStateId) {
            this.missionStateId = missionStateId;
        }

        public String getMissionStateId() {
            return missionStateId;
        }

        public static MissionState fromId(String givenId) {
            for (MissionState missionState : MissionState.values()) {
                if (givenId.equals(missionState.getMissionStateId())) {
                    return missionState;
                }
            }
            return null;
        }
    }

    public enum ErrorState {
        NONE("0"),
        GPS_ERROR("1");

        private final String errorId;

        ErrorState(String errorId) {
            this.errorId = errorId;
        }

        public String getErrorId() {
            return errorId;
        }

        public static ErrorState fromId(String givenId) {
            for (ErrorState errorState : ErrorState.values()) {
                if (givenId.equals(errorState.getErrorId())) {
                    return errorState;
                }
            }
            return null;
        }
    }

    private int auvId;
    private double logitude;
    private double latitude;
    private double heading;
    private double batteryVoltage;
    private MissionState missionState;
    private ErrorState errorState;

    public AuvsmMessage(int auvId, double logitude, double latitude, double heading, double batteryVoltage, MissionState missionState, ErrorState errorState) {
        this.auvId = auvId;
        this.logitude = logitude;
        this.latitude = latitude;
        this.heading = heading;
        this.batteryVoltage = batteryVoltage;
        this.missionState = missionState;
        this.errorState = errorState;
    }

    @Override
    protected void generateMessageContentsWithoutId(StringBuilder messageBuilder) {
        messageBuilder.append(auvId);
        messageBuilder.append(",");
        messageBuilder.append(String.format("%.8f", logitude));
        messageBuilder.append(",");
        messageBuilder.append(String.format("%.8f", latitude));
        messageBuilder.append(",");
        messageBuilder.append(String.format("%.2f", heading));
        messageBuilder.append(",");
        messageBuilder.append(String.format("%.2f", batteryVoltage));
        messageBuilder.append(",");
        messageBuilder.append(missionState.getMissionStateId());
        messageBuilder.append(",");
        messageBuilder.append(errorState.getErrorId());
    }


    public AuvsmMessage(List<String> messageValues) throws InvalidMessageException {
        super(messageValues);

        if (messageValues.size() != 7) {
            throw new InvalidMessageException("Expected 2 message values, received: " + messageValues.size());
        }

        String receivedAuvIdString = messageValues.get(0);
        String receivedLongitudeString = messageValues.get(1);
        String receivedLatitudeString = messageValues.get(2);
        String receivedHeadingString = messageValues.get(3);
        String receivedBatteryVoltageString = messageValues.get(4);
        String receivedMissionStateString = messageValues.get(5);
        String receivedErrorStateString = messageValues.get(6);

        try {
            this.auvId = Integer.parseInt(receivedAuvIdString);
        } catch (NumberFormatException e) {
            throw new InvalidMessageException("Invalid auvId, received: " + receivedAuvIdString, e);
        }

        try {
            this.logitude = Double.parseDouble(receivedLongitudeString);
        } catch (NumberFormatException e) {
            throw new InvalidMessageException("Invalid longitude, received: " + receivedLongitudeString);
        }

        try {
            this.latitude = Double.parseDouble(receivedLatitudeString);
        } catch (NumberFormatException e) {
            throw new InvalidMessageException("Invalid latitude, received: " + receivedLatitudeString);
        }

        try {
            this.heading = Double.parseDouble(receivedHeadingString);
        } catch (NumberFormatException e) {
            throw new InvalidMessageException("Invalid heading, received: " + receivedHeadingString);
        }

        try {
            this.batteryVoltage = Double.parseDouble(receivedBatteryVoltageString);
        } catch (NumberFormatException e) {
            throw new InvalidMessageException("Invalid batteryVoltage, received: " + receivedBatteryVoltageString);
        }

        this.missionState = MissionState.fromId(receivedMissionStateString);
        if (this.missionState == null) {
            throw new InvalidMessageException("Unknown MissionState, received: " + receivedMissionStateString);
        }

        this.errorState = ErrorState.fromId(receivedErrorStateString);
        if (this.errorState == null) {
            throw new InvalidMessageException("Unknown ErrorState, received: " + receivedMissionStateString);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuvsmMessage that = (AuvsmMessage) o;

        if (auvId != that.auvId) return false;
        if (Double.compare(that.logitude, logitude) != 0) return false;
        if (Double.compare(that.latitude, latitude) != 0) return false;
        if (Double.compare(that.heading, heading) != 0) return false;
        if (Double.compare(that.batteryVoltage, batteryVoltage) != 0) return false;
        if (missionState != that.missionState) return false;
        return errorState == that.errorState;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = auvId;
        temp = Double.doubleToLongBits(logitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(latitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(heading);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(batteryVoltage);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (missionState != null ? missionState.hashCode() : 0);
        result = 31 * result + (errorState != null ? errorState.hashCode() : 0);
        return result;
    }
}
