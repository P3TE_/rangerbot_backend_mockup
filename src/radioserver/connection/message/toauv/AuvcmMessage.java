package radioserver.connection.message.toauv;

import radioserver.connection.message.InvalidMessageException;
import radioserver.connection.message.MessageBase;

import java.util.List;

/**
 * @author p3te
 */
public class AuvcmMessage extends MessageBase {

    public static final String MESSAGE_ID = "AUVCM";

    @Override
    protected String getMessageId() {
        return MESSAGE_ID;
    }

    public enum AuvAction {
        STOP("0"),
        START("1"),
        PAUSE("2");

        private final String actionId;

        AuvAction(String actionId) {
            this.actionId = actionId;
        }

        public String getActionId() {
            return actionId;
        }

        public static AuvAction fromId(String givenId) {
            for (AuvAction auvAction : AuvAction.values()) {
                if (givenId.equals(auvAction.getActionId())) {
                    return auvAction;
                }
            }
            return null;
        }
    }

    private int auvId;
    private AuvAction auvAction;

    public AuvcmMessage(int auvId, AuvAction auvAction) {
        this.auvId = auvId;
        this.auvAction = auvAction;
    }

    @Override
    protected void generateMessageContentsWithoutId(StringBuilder messageBuilder) {
        messageBuilder.append(auvId);
        messageBuilder.append(",");
        messageBuilder.append(auvAction.getActionId());
    }


    public AuvcmMessage(List<String> messageValues) throws InvalidMessageException {
        super(messageValues);

        if (messageValues.size() != 2) {
            throw new InvalidMessageException("Expected 2 message values, received: " + messageValues.size());
        }

        String receivedAuvIdString = messageValues.get(0);
        String receivedActionId = messageValues.get(1);
        try {
            this.auvId = Integer.parseInt(receivedAuvIdString);
        } catch (NumberFormatException e) {
            throw new InvalidMessageException("Expected auvId, received: " + receivedAuvIdString, e);
        }

        this.auvAction = AuvAction.fromId(receivedActionId);
        if (this.auvAction == null) {
            throw new InvalidMessageException("Unknown Auv action with id: " + receivedActionId);
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuvcmMessage that = (AuvcmMessage) o;

        if (auvId != that.auvId) return false;
        return auvAction == that.auvAction;
    }

    @Override
    public int hashCode() {
        int result = auvId;
        result = 31 * result + (auvAction != null ? auvAction.hashCode() : 0);
        return result;
    }
}
