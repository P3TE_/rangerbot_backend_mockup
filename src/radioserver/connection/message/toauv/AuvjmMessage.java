package radioserver.connection.message.toauv;

import radioserver.connection.message.InvalidMessageException;
import radioserver.connection.message.MessageBase;

import java.util.List;

/**
 * @author p3te
 */
public class AuvjmMessage extends MessageBase {

    public static final String MESSAGE_ID = "AUVJM";

    @Override
    protected String getMessageId() {
        return MESSAGE_ID;
    }

    private int auvId;
    private double forwardBackwards;
    private double leftRight;

    public AuvjmMessage(int auvId, double forwardBackwards, double leftRight) {
        this.auvId = auvId;
        this.forwardBackwards = forwardBackwards;
        this.leftRight = leftRight;
    }

    @Override
    protected void generateMessageContentsWithoutId(StringBuilder messageBuilder) {
        messageBuilder.append(auvId);
        messageBuilder.append(",");
        messageBuilder.append(String.format("%.2f", forwardBackwards));
        messageBuilder.append(",");
        messageBuilder.append(String.format("%.2f", leftRight));
    }


    public AuvjmMessage(List<String> messageValues) throws InvalidMessageException {
        super(messageValues);

        if (messageValues.size() != 3) {
            throw new InvalidMessageException("Expected 2 message values, received: " + messageValues.size());
        }

        String receivedAuvIdString = messageValues.get(0);
        String receivedForwardBackwards = messageValues.get(1);
        String receivedLeftRight = messageValues.get(2);
        try {
            this.auvId = Integer.parseInt(receivedAuvIdString);
        } catch (NumberFormatException e) {
            throw new InvalidMessageException("Expected auvId, received: " + receivedAuvIdString, e);
        }

        try {
            this.forwardBackwards = Double.parseDouble(receivedForwardBackwards);
        } catch (NumberFormatException e) {
            throw new InvalidMessageException("Invalid forwardBackwards, expected floating point number, received: " + receivedForwardBackwards, e);
        }

        try {
            this.leftRight = Double.parseDouble(receivedLeftRight);
        } catch (NumberFormatException e) {
            throw new InvalidMessageException("Invalid LeftRight, expected floating point number, received: " + receivedLeftRight, e);
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuvjmMessage that = (AuvjmMessage) o;

        if (auvId != that.auvId) return false;
        if (Double.compare(that.forwardBackwards, forwardBackwards) != 0) return false;
        return Double.compare(that.leftRight, leftRight) == 0;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = auvId;
        temp = Double.doubleToLongBits(forwardBackwards);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(leftRight);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
