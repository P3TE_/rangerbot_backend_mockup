package radioserver.dummyuav;

import radioserver.connection.TcpClient;
import radioserver.connection.message.fromauv.AuvsmMessage;

public class DummyAuv {

    private final int uavId;
    private String uavName;
    private double latitude;
    private double longitude;
    private double heading; //Relative to true north.
    private double batteryVoltage; //In volts
    private String ip = null;
    private AuvsmMessage.MissionState missionState = AuvsmMessage.MissionState.STOPPED;
    private AuvsmMessage.ErrorState errorState = AuvsmMessage.ErrorState.NONE;

    public DummyAuv(int uavId, String uavName, double latitude, double longitude, double heading, double batteryVoltage, String ip) {
        this.uavId = uavId;
        this.uavName = uavName;
        this.latitude = latitude;
        this.longitude = longitude;
        this.heading = heading;
        this.batteryVoltage = batteryVoltage;
        this.ip = ip;
        DummyAuvHandler.addDummyAuv(this);
    }

    public AuvsmMessage generateStatusMessage() {
        return new AuvsmMessage(uavId, longitude, latitude, heading, batteryVoltage, missionState, errorState);
    }

    public int getUavId() {
        return uavId;
    }

    public String getUavName() {
        return uavName;
    }

    public void setUavName(String uavName) {
        this.uavName = uavName;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public double getHeading() {
        return heading;
    }

    public void setHeading(double heading) {
        this.heading = heading;
    }

    public double getBatteryVoltage() {
        return batteryVoltage;
    }

    public void setBatteryVoltage(double batteryVoltage) {
        this.batteryVoltage = batteryVoltage;
    }

    public AuvsmMessage.MissionState getMissionState() {
        return missionState;
    }

    public void setMissionState(AuvsmMessage.MissionState missionState) {
        this.missionState = missionState;
    }

    public AuvsmMessage.ErrorState getErrorState() {
        return errorState;
    }

    public void setErrorState(AuvsmMessage.ErrorState errorState) {
        this.errorState = errorState;
    }

    public void handleNewManualControlMessage(TcpClient client, double direction, double speed) {

    }
}
