package radioserver.dummyuav;

import java.util.HashMap;

public class DummyAuvHandler {

    private static HashMap<Integer, DummyAuv> allAuvs = new HashMap<>();

    public static void addDummyAuv(DummyAuv dummyAuv) {
        allAuvs.put(dummyAuv.getUavId(), dummyAuv);
    }

    public static DummyAuv getDummyAuv(int uavId) {
        return allAuvs.get(uavId);
    }

}
