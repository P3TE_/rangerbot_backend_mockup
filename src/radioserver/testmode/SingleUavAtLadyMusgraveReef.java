package radioserver.testmode;

import radioserver.connection.TcpServer;
import radioserver.dummyuav.DummyAuv;

public class SingleUavAtLadyMusgraveReef extends TestMode {

    private volatile boolean running = true;
    private Thread runningThread = null;

    private double currentRotation = 0;
    private static final double ROTATION_INCREASE = Math.PI / 4.0;
    private static final double LAT_LONG_RADIUS = 0.00001; //1.11 m
    private static final double startLat = -23.904488;
    private static final double startLong = 152.411627;

    private DummyAuv dummyAuv3 = new DummyAuv(3, "nautalus", startLat, startLong,
            convertAngleToHeading(currentRotation), 11.3, "45.63.24.145");

    public SingleUavAtLadyMusgraveReef(TcpServer tcpServer) {
        super(tcpServer);
    }

    @Override
    public void run() {
        runningThread = Thread.currentThread();
        while (running) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                //Probably told to stop.
                continue;
            }

            //Have the uav go around in a circle:
            currentRotation += ROTATION_INCREASE;
            dummyAuv3.setLatitude(startLat + (LAT_LONG_RADIUS * Math.cos(currentRotation)));
            dummyAuv3.setLongitude(startLong + (LAT_LONG_RADIUS * Math.sin(currentRotation)));

            //Generate and send the uav message.
            this.tcpServer.sendMessageToAllClients(dummyAuv3.generateStatusMessage());

        }
    }
}
