package radioserver.testmode;

import radioserver.connection.TcpServer;
import radioserver.connection.message.fromauv.AuvsmMessage;
import radioserver.dummyuav.DummyAuv;

public class SingleUavAtOriginTestMode extends TestMode {

    private volatile boolean running = true;
    private Thread runningThread = null;

    private double currentRotation = 0;
    private static final double ROTATION_INCREASE = Math.PI / 4.0;
    private static final double LAT_LONG_RADIUS = 0.00001; //1.11 m

    private DummyAuv dummyAuv = new DummyAuv(1, "subby mc sub face", 0, 0,
            convertAngleToHeading(currentRotation), 15.0, "45.63.24.145");

    public SingleUavAtOriginTestMode(TcpServer tcpServer) {
        super(tcpServer);
    }

    @Override
    public void run() {
        runningThread = Thread.currentThread();
        while (running) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                //Probably told to stop.
                continue;
            }

            //Have the uav go around in a circle:
            currentRotation += ROTATION_INCREASE;
            dummyAuv.setMissionState(AuvsmMessage.MissionState.STOPPED);
            dummyAuv.setHeading(convertAngleToHeading(currentRotation));
            dummyAuv.setLatitude(LAT_LONG_RADIUS * Math.cos(currentRotation));
            dummyAuv.setLongitude(LAT_LONG_RADIUS * Math.sin(currentRotation));

            //Generate and send the uav message.
            this.tcpServer.sendMessageToAllClients(dummyAuv.generateStatusMessage());

        }
    }
}
