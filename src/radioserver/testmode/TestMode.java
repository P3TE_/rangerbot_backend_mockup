package radioserver.testmode;

import radioserver.connection.TcpServer;

public abstract class TestMode implements Runnable {

    public static double convertAngleToHeading(double angle) {
        return Math.toDegrees(angle + (Math.PI / 2.0));
    }

    protected TcpServer tcpServer;

    public TestMode(TcpServer tcpServer) {
        this.tcpServer = tcpServer;
    }
}
