package radioserver.testmode;

import radioserver.connection.TcpServer;

import java.util.Scanner;

public enum TestModeType {
    SIMPLE_TEST_ONE_UAV_AT_ORIGIN,
    SINGLE_UAV_AT_LADY_MUSGRAVE_REEF,
    TWO_UAVS_AT_HERRON_REEF;


    public TestMode generateTestMode(TcpServer tcpServer) {
        switch (this) {
            case SIMPLE_TEST_ONE_UAV_AT_ORIGIN:
                return new SingleUavAtOriginTestMode(tcpServer);
            case SINGLE_UAV_AT_LADY_MUSGRAVE_REEF:
                return new SingleUavAtLadyMusgraveReef(tcpServer);
            case TWO_UAVS_AT_HERRON_REEF:
                return new TwoUavsAtHerronReef(tcpServer);
        }
        return null;
    }




    public static TestModeType getTestModeFromUseringInput(Scanner scanner) {
        TestModeType testModeType = null;

        while (testModeType == null) {

            System.out.println();
            System.out.println("Please enter test mode:");
            System.out.println("Valid test modes include: ");
            for (int i = 0; i < TestModeType.values().length; i++) {
                TestModeType testModeTypeOption = TestModeType.values()[i];
                System.out.println(i + " : " + testModeTypeOption.name());
            }

            System.out.print("> ");
            System.out.flush();

            String userLine = scanner.nextLine().trim();

            int userLineValue;
            try {
                userLineValue = Integer.parseInt(userLine);
            } catch (NumberFormatException e) {
                System.err.println("Please enter a valid integer, received: " + userLine);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                continue;
            }

            if ((userLineValue < 0) || (userLineValue >= TestModeType.values().length)) {
                //Invalid input.
                System.err.println("Please enter a number between 0 and " + TestModeType.values().length);
                System.out.println("Valid options include: ");

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                continue;
            } else {
                testModeType = TestModeType.values()[userLineValue];
            }
        }

        System.out.println("Using test mode: " + testModeType.ordinal() + " : " + testModeType.name());

        return testModeType;
    }
}
