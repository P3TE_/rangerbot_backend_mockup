package radioserver.testmode;

import radioserver.connection.TcpServer;
import radioserver.connection.message.fromauv.AuvsmMessage;
import radioserver.dummyuav.DummyAuv;

public class TwoUavsAtHerronReef extends TestMode {

    private volatile boolean running = true;
    private Thread runningThread = null;

    private double currentRotationBot1 = 0;
    private double currentRotationBot2 = 0;
    private static final double ROTATION_INCREASE = Math.PI / 4.0;
    private static final double LAT_LONG_RADIUS = 0.00001; //1.11 m


    private static final double startLat1 = -23.451305;
    private static final double startLong1 = 151.958511;
    private static final double startLat2 = -23.453355;
    private static final double startLong2 = 151.942919;

    private DummyAuv dummyAuv1 = new DummyAuv(1, "subby mc sub face", startLat1, startLong1,
            convertAngleToHeading(currentRotationBot1), 11.5, null);
    private DummyAuv dummyAuv3 = new DummyAuv(3, "nautalus", startLat2, startLong2,
            convertAngleToHeading(currentRotationBot1), 9.76, "45.63.24.145");

    public TwoUavsAtHerronReef(TcpServer tcpServer) {
        super(tcpServer);
    }

    @Override
    public void run() {
        runningThread = Thread.currentThread();
        while (running) {
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                //Probably told to stop.
                continue;
            }

            //Have the uav go around in a circle:
            currentRotationBot1 += ROTATION_INCREASE;


            dummyAuv1.setHeading(convertAngleToHeading(currentRotationBot1));
            dummyAuv1.setMissionState(AuvsmMessage.MissionState.PAUSED);
            dummyAuv1.setLatitude(startLat1 + (LAT_LONG_RADIUS * Math.cos(currentRotationBot1)));
            dummyAuv1.setLongitude(startLong1 + (LAT_LONG_RADIUS * Math.sin(currentRotationBot1)));

            //Generate and send the uav message.
            this.tcpServer.sendMessageToAllClients(dummyAuv1.generateStatusMessage());


            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                //Probably told to stop.
                continue;
            }

            currentRotationBot2 += (ROTATION_INCREASE / 2.0);
            dummyAuv3.setHeading(convertAngleToHeading(currentRotationBot2));
            dummyAuv3.setLatitude(startLat2 + (LAT_LONG_RADIUS * Math.cos(currentRotationBot2)));
            dummyAuv3.setLongitude(startLong2 + (LAT_LONG_RADIUS * Math.sin(currentRotationBot2)));
            dummyAuv3.setErrorState(AuvsmMessage.ErrorState.GPS_ERROR);

            //Generate and send the uav message.
            this.tcpServer.sendMessageToAllClients(dummyAuv3.generateStatusMessage());

        }
    }
}
